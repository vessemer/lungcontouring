#include <octave/oct.h>
#include <vector>
#include <climits>
#include <queue>
#include <set>
#include <utility>   
#include <algorithm>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include <assert.h>
#include <typeinfo>
#include <iostream>
#include <fstream>

#define NUM_FRAGMENT 10
#define SQR(x) ((x) * (x))
#define QB(x) ((x) * (x) * (x))
#define SIGMA 0.021737
#define SIZE_X 2570
#define SIZE_Y 2489
#define SIGMA_SQR -400
#define BASE 3




//TODO: Interpolate all structures for n-dimension case
//Represent coordinates as list 
//Implement typical methods for all structures


template <typename T> struct Point {
  T x, y;
  Point() {}
  Point(T x, T y) : x(x), y(y) {}
  void shift(T a, T b) {
    x += a;
    y += b;
  }

  void add(Point<T> &p) {
    x += p.x;
    y += p.y;
  }

  inline void inverse() {
    std::swap(x, y);
  }


  inline double operator* (const Point<double> &point) const {
    return (double)x * point.x + (double)y * point.y;
  }

  inline Point<double> operator+ (const Point<double> &point) const {
    return Point<double> (x + point.x, y + point.y);
  }

  inline Point<double> operator- (const Point<double> &point) const {
    return Point<double> (x - point.x, y - point.y);
  }

  inline void operator+= (const Point<double> &point) {
    x += point.x;
    y += point.y;
  }

  inline void operator/= (const Point<double> &point) {
    x /= point.x ? point.x : 1;
    y /= point.y ? point.y : 1;
  }

  inline void operator/= (const double &rgh) {
    x /= rgh ? rgh : 1;
    y /= rgh ? rgh : 1;
  }

  inline void operator*= (const double &rgh) {
    x *= rgh;
    y *= rgh;
  }

  inline void operator*= (const Point<double> &point) {
     x *= point.x;
     y *= point.y;
  }

  inline double norm() const {
    return sqrt(SQR(x) + SQR(y));
  }

  inline void multip(T time) {
    x *= time;
    y *= time;
  }

  inline bool isCoveredBy(const Point<T> &c) const {
    return !(c.y + 1 < x || y + 1 < c.x); 
  }

  inline T getDistanceTo(const Point<T> &point) const {
    return SQR(point.x - x) + SQR(point.y - y);
  }

  // inline double calcPotential(const Point<T> &target) {
  //   double dist = 1.0 / getDistanceTo(target);
  //   return  dist != 0 ? FI * dist : 0;
  // }

  inline Point<double> calcTension(const Point<T> &target, double k) {
    double i = y - target.y;
    double j = target.x - x;
    double dist = sqrt(getDistanceTo(target));
    dist = SIGMA * (1.0 - k * dist) / (1.0 + 5 * k * dist);
    double normalize = sqrt(SQR(i) + SQR(j));
    dist /= abs(normalize) < 0.01 ? 1 : normalize;
    i*= dist;
    j*= dist;

    return  Point<double>(i, j);
  }

  inline double calcPotential(const Point<T> &target) {
    int sdx = SQR(target.x - x);
    int sdy = SQR(target.y - y);
    return  std::pow(BASE, (sdx + sdy) / SIGMA_SQR);
  }

  // inline Point<double> calcTension(const Point<T> &target) {
  //   int sdx = SQR(target.x - x);
  //   int sdy = SQR(target.y - y);
  //   double i = std::pow(BASE, (sdx + sdy) / SIGMA_SQR);
  //   return  Point<double>(i, i);
  // }  
};


  
inline Point<double> const sum(const Point<double> &fst, 
  const Point<double> &snd) {
  return Point<double> (fst.x + snd.x, fst.y + snd.y);
}


inline Point<double> const prod(const Point<double> &point, const double &k) {
  return Point<double>(k * point.x, k * point.y);
}



inline Point<double> const normalized(const Point<double> &point) {
  double ds = sqrt(SQR(point.x) + SQR(point.y));
  ds = abs(ds) > 1e-09 ? ds : 1.0;
  return Point<double> (point.x / ds, point.y / ds);
}


struct Bezier {
  Point<double> Q0, Q1, Q2, Q3;

  Bezier() {}
  Bezier(const Point<double> &Q0, const Point<double> &Q1, 
    const Point<double> &Q2, const Point<double> &Q3) 
    : Q0(Q0), Q1(Q1), Q2(Q2), Q3(Q3) { }


  inline Point<double> getBezier(const double t) const {
    return  sum(sum(sum(prod(Q0, QB(1 - t)),  
                 prod(Q1, 3 * t * SQR(1 - t))),
             prod(Q2, 3 * SQR(t) * (1 - t))), 
         prod(Q3, QB(t)));
  }


  inline Point<double> getBezierDeravative(const double t) const {
    return sum(sum(sum(prod(Q0, -3 * SQR(1 - t)),  
                 prod(Q1, 9 * SQR(t) - 12 * t + 3)),
             prod(Q2, 6 * t - 9 * SQR(t))), 
         prod(Q3, 3 * SQR(t)));
  }
};


struct Agregator {
  double components[NUM_FRAGMENT] = {0.0};
  inline void inc(const int i, const double d) {
    components[i]+= d;
  }

  int max() const {
    double max = -SIZE_X;
    int pos = -1;
    for (int i = 0; i < NUM_FRAGMENT; ++i) 
      if (components[i] > max) {
        max = components[i];
        pos = i;
      }

    return pos;
  }
};


struct LinearBezier {
  Point<double> Q0, Q1;
  LinearBezier() { }
  LinearBezier(const Point<int> &Q0, const Point<int> &Q1) 
    : Q0(Point<double>(Q0.x, Q0.y)), Q1(Point<double>(Q1.x, Q1.y)) { }

  inline Point<double> const getBezier(const double t) {
    return  sum(prod(Q0, (1 - t)),
              prod(Q1, t));
  }
};


template <typename T = int> struct Island {
  double level;
  T center;
  std::vector<T> points;
  std::pair<Agregator, Agregator> agregatorPairs; 
  Agregator agregator;
  std::pair<Bezier, Bezier> BezierPairs; 
  Bezier bezier;
  Point<double> angle;

  Island() : Island(0.0) {}
  Island(double i) : level(i) {}
  Island(T point) : Island(0.0, point) {}
  Island(double i, T center) : level(i) {
    points.push_back(center);
  }

  Island(const Island<T> &island1, const Island<T> &island2) {
    points.insert(points.end(), island1.points.begin(),
                   island1.points.end());
    points.insert(points.end(), island2.points.begin(),
                   island2.points.end());
  }

  ~Island() { points.clear(); }

  void operator+=(const Island<T> *island) {    
    points.insert(points.end(), island->points.begin(),
                   island->points.end());
  }

  Island<T> add(T point) {
    points.push_back(point);
    return *this;
  }

  Island<T> add(const Island<T> &island) {
    points.insert(points.end(), island.points.begin(),
                   island.points.end());
    return *this;
  }
};


Island<Point<int>> generateShape(const Point<int> &center) {
  Island<Point<int>> shape(center);
  for (int i = -50; i < 50; ++i) {
    shape.points.push_back(Point<int>(center.x - i, center.y));
    shape.points.push_back(Point<int>(center.x, center.y - i));
  }

  shape.bezier = Bezier(Point<double>(center.x - 50, center.y), 
                        Point<double>(center.x, center.y),
                        Point<double>(center.x, center.y),
                        Point<double>(center.x +50, center.y));
  return shape;
}

struct Fragment {
  Fragment() { }
}; 

struct Diofragm : Fragment {};

struct BottomBorderRightLung : Fragment {};

struct RightBorderRightLung : Fragment {};

struct TopBorderRightLung : Fragment {};

struct LeftBorderRightLung : Fragment {};

struct BottomBorderLeftLung : Fragment {};

struct RightBorderLeftLung : Fragment {};

struct TopBorderLeftLung : Fragment {};

struct LeftBorderLeftLung : Fragment {};



struct JSON {
  Matrix Contours[NUM_FRAGMENT];

  JSON(const octave_value_list &JSON, int init) {
    for (int i = init; i < JSON.length(); ++i) {
      if ((i - init) % 2) 
        Contours[(i - init) / 2] = JSON(i).matrix_value().stack(Contours[(i - init) / 2]);    
      else
        Contours[(i - init) / 2] = JSON(i).matrix_value();  
    }
  }
};

        
typedef std::vector<Island<std::pair<Point<int>, Point<int>>>> line;
//===============================================================================================================


void bfs(Matrix &G, Point<int> point, 
  const std::function<void(Point<int>)> &execute, 
  const std::function<bool(const Point<int>&)> &terminate, 
  const std::function<bool(const double&)> &predicate) {
  

  std::queue<Point<int>> queue;
  queue.push(point);
  Matrix mask(G.rows(), G.cols());
  mask.fill(0.0);
  mask(point.x, point.y) = 1;

  if (point.x >= G.rows() - 1|| point.y >= G.cols() - 1
      || point.x < 1 || point.y < 1)
    return;

  while (!queue.empty() && !terminate(point)) {
      point = queue.front();
      queue.pop();
      execute(point);

      if (point.x > 0) {
        if (predicate(G(point.x - 1, point.y)) && !mask(point.x - 1, point.y)) { 
          queue.push(Point<int>(point.x -1, point.y));
          mask(point.x - 1, point.y) = 1;
        }
        if (point.y > 0 && predicate(G(point.x - 1, point.y - 1)) && !mask(point.x - 1, point.y - 1)) {
          queue.push(Point<int>(point.x -1, point.y - 1));
          mask(point.x - 1, point.y - 1) = 1;
        }
        if (point.y < G.cols() - 1 && predicate(G(point.x - 1, point.y + 1)) && !mask(point.x - 1, point.y + 1)) {
          queue.push(Point<int>(point.x -1, point.y + 1));
          mask(point.x - 1, point.y + 1) = 1;
        }
      }
      if (point.y > 0 && predicate(G(point.x, point.y - 1)) && !mask(point.x, point.y - 1)) {
        queue.push(Point<int>(point.x, point.y - 1));
        mask(point.x, point.y - 1) = 1;
      }
      if (point.y < G.cols() - 1 && predicate(G(point.x, point.y + 1)) && !mask(point.x, point.y + 1)) {
        queue.push(Point<int>(point.x, point.y + 1));
        mask(point.x, point.y + 1) = 1;
      }
      
      if (point.x < G.rows() - 1) {
        if (predicate(G(point.x + 1, point.y)) && !mask(point.x + 1, point.y)) {
          queue.push(Point<int>(point.x + 1, point.y));
          mask(point.x + 1, point.y) = 1;
        }
        if (point.y < G.cols() - 1 && predicate(G(point.x + 1, point.y + 1)) && !mask(point.x + 1, point.y + 1)) {
          queue.push(Point<int>(point.x + 1, point.y + 1));
          mask(point.x + 1, point.y + 1) = 1;
        }
        if (point.y > 0 && predicate(G(point.x + 1, point.y - 1)) && !mask(point.x + 1, point.y - 1)) {
          queue.push(Point<int>(point.x + 1, point.y - 1));
          mask(point.x + 1, point.y - 1) = 1;
        }
      }
  }
}


//===============================================================================================================

struct StraightLine {
  double a, b, c;

  StraightLine(const Point<int> &fst, const Point<int> &snd) {
    double dist = snd.x - fst.x;
    dist = dist? dist : 1.0;
    a = 1.0 / dist;
    
    dist = fst.y - snd.y;
    dist = dist? dist : 1.0;
    b = 1.0 / dist;
    
    c = -fst.x * a - fst.y * b;
  }

  StraightLine(const Point<double> &fst, const Point<double> &snd) {
    double dist = snd.x - fst.x;
    dist = dist? dist : 1.0;
    a = 1.0 / dist;

    dist = fst.y - snd.y;
    dist = dist? dist : 1.0;
    b = 1.0 / dist;
    
    c = -fst.x * a - fst.y * b;
  }
};


inline double getDistanceToLine(const Point<int> &point, 
  const StraightLine &line) {
  return (line.a * point.x + line.b * point.y + line.c) 
  / sqrt(SQR(line.a) + SQR(line.b));
}

inline double getDistanceToLine(const Point<double> &point, 
  const StraightLine &line) {
  double dist = sqrt(SQR(line.a) + SQR(line.b));
  dist = dist? dist : 1;
  return (line.a * point.x + line.b * point.y + line.c) 
  / dist;
}



bool angleComparator(const Point<double> &lft, 
                     const Point<double> &rgh, 
                     const Point<double> &center) {

  std::pair<Point<double>, Point<double>>
                pair(lft - center, rgh - center);

  return (pair.first.x * pair.second.y 
         -pair.first.y * pair.second.x) > 0;
}


bool angleComparator(const Island<Point<int>> &lft, 
                     const Island<Point<int>> &rgh, 
                     const Point<double> &center) {

  return (angleComparator(rgh.bezier.Q0, lft.bezier.Q0, center)
          && angleComparator(rgh.bezier.Q0, lft.bezier.Q3, center)
          && angleComparator(rgh.bezier.Q3, lft.bezier.Q0, center)
          && angleComparator(rgh.bezier.Q3, lft.bezier.Q3, center));
}


bool isCovered(const Island<Point<int>> &lft, 
               const Island<Point<int>> &rgh, 
               const Point<double> &center,
               const double threshold = 0.0) {

  return !(angleComparator(rgh, lft, center)
            || angleComparator(lft, rgh, center)); 
}


std::pair<Point<int>, Point<int>> topBottomPoint(const StraightLine &line, 
  const std::vector<Point<int>> &points) {
  
  std::pair<Point<int>, Point<int>> pair(points.front(), points.back());
  double bottomDis = 10000.0;
  double topDis = -10000.0;

  for (point :points) {
    double dist = getDistanceToLine(point, line);
    if (dist > topDis) {
      pair.first = point;
      topDis = dist;
    } else if(dist < bottomDis) {
      pair.second = point;
      bottomDis = dist;
    }
  }

  return pair;
}


inline void BezierSwap(Point<int> &base, Point<int> &top, Point<int> &bottom) {  
    if (base.getDistanceTo(top) 
      > base.getDistanceTo(bottom)) {
      std::swap(top, bottom);
  }
}


std::pair<Point<int>, Point<int>> maxDist(const Island<Point<int>> island) {
  std::pair<Point<int>, Point<int>> inst;
  int dist = -1;

  if (island.points.size() > 3)
    for (auto i = island.points.begin(); i != island.points.end() - 1; i++)
      for (auto j = i + 1; j != island.points.end(); j++) 
        if (i->getDistanceTo(*j) > dist) {
          dist = i->getDistanceTo(*j);
          inst = std::make_pair(*i, *j);
        }       
  return inst; 
}


std::vector<Island<Point<int>>> zip(const line &line) {
  std::vector<Island<Point<int>>> lines;
  for (isld :line) {
    Island<Point<int>> island; 
    for (pointPair :isld.points) {
      island.points.push_back(pointPair.first);
      island.points.push_back(pointPair.second);
    }
    lines.push_back(island);
  }
  return lines;
} 


void BezierApproximation(std::vector<Island<Point<int>>> &lines) {
  std::pair<Point<int>, Point<int>> fronts;
  std::pair<Point<int>, Point<int>> topBottom;

  for (island :lines) {
    fronts = maxDist(island);
 
    StraightLine straightLines(fronts.first, fronts.second);
    topBottom = topBottomPoint(straightLines, island.points);
    BezierSwap(fronts.first, topBottom.first, topBottom.second);

    island.bezier = 
      Bezier(Point<double>((double)fronts.first.x, (double)fronts.first.y), 
             Point<double>((double)topBottom.first.x, (double)topBottom.first.y),
             Point<double>((double)topBottom.second.x, (double)topBottom.second.y),
             Point<double>((double)fronts.second.x, (double)fronts.second.y));
  }
}



// void BezierApproximation(line &line) {
//   std::pair<Point<int>, Point<int>> front, back;
//   std::pair<Point<int>, Point<int>> top, bottom;
//   std::vector<Island<Point<int>>> lines = zip(line); 

//   for (island :line) {
//     std::pair<double, double> topDis(0.0, 0.0), bottomDis(0.0, 0.0);
//     front = island.points.front();
//     back = island.points.back();
//     top = island.points.front();
//     bottom = island.points.back(); 
//     std::pair<StraightLine, StraightLine> 
//       straightLines(StraightLine(front.first, back.first), 
//                     StraightLine(front.second, back.second));

//     for (point :island.points) {
//       topBottomPoint(straightLines.first, point.first, topDis.first, 
//         bottomDis.first, top.first, bottom.first);

//       topBottomPoint(straightLines.second, point.second, topDis.second, 
//         bottomDis.second, top.second, bottom.second);
//     }
//     BezierSwap(front.first, top.first, bottom.first);
//     BezierSwap(front.second, top.second, bottom.second);

//     island.BezierPairs = 
//       std::make_pair(Bezier(front.first, top.first, 
//           bottom.first, back.first), 
//         Bezier(front.second, top.second, 
//           bottom.second, back.second));
//   }
// }




//===============================================================================================================


inline bool isCoveredBy(const std::pair<Point<int>, Point<int>> &top, 
  const std::pair<Point<int>, Point<int>> &dwn) {

  return !(dwn.second.x + 1 < top.first.x 
    || top.second.x + 1 < dwn.first.x); 
}


void splitLayer(std::vector<Point<int>>::iterator &i, 
  const std::vector<Point<int>>::iterator end, 
  line &components) {
  
  Point<int> fst, snd;
  fst.y = i->y;
  snd.y = i->y;

  for (; ; ++i) {
    if (fst.y != i->y
      || i == end){
      --i;
      return;
    }

    fst.x = i->x;
    for (; ; ++i) {
      if (((i->x != fst.x) && (i->x != (i - 1)->x + 1)) 
          || fst.y != i->y 
          || i == end) {
        --i;
        break;
      }
    }
    snd.x = i->x;
    components.push_back(Island<std::pair<Point<int>, Point<int>>>(std::make_pair(fst, snd)));
  }  
}


void finallySplitItDown(line &components, 
  line &prevComponents, line &lines) {
  line buff;
  line pull;

  for (comp :components) {
    pull.clear();
    for (p :prevComponents)
      if (isCoveredBy(comp.points.front(), p.points.back())) 
          pull.push_back(p);


    for (p :pull)
      p.add(comp);


    if (pull.size() <= 1) {
      if (pull.size())
        comp = pull.front();
      continue;
    }
    comp = pull.front();

    for (auto p = pull.begin(); p != pull.end(); ++p) {
      for (auto q = p + 1; q != pull.end(); ++q) 
          lines.push_back(Island<std::pair<Point<int>, Point<int>>>(*p, *q));

      if (p == pull.end() - 2) 
        break;

      if (p != pull.begin())
        buff.push_back(Island<std::pair<Point<int>, 
          Point<int>>>(*p));
    }
  }
  components.insert(components.end(), buff.begin(), buff.end());
  buff.clear();
}


line splitByLines(std::vector<Point<int>> &points) {
  line prevComponents;
  line components;
  line lines;
  

  std::sort(points.begin(), points.end(), 
    [](const Point<int> &lft, const Point<int> &rgh) -> bool {
      return lft.y < rgh.y ? 
        true : lft.y > rgh.y ? false : lft.x < rgh.x; 
    });

  for (auto i = points.begin(); i != points.end(); ++i) {
    splitLayer(i, points.end(), components);      
    finallySplitItDown(components, prevComponents, lines);
    prevComponents.clear();
    prevComponents.swap(components);
  }

  for (comp: prevComponents)
    lines.push_back(comp);

  return lines;
}


//===============================================================================================================


void inverse(std::vector<Island<Point<int>>> &line) {
  for (island :line)
    for (point :island.points)
      point.inverse();
}


void inverse(line &line) {
  for (island :line)
    for (point :island.points) {
      point.second.inverse();
      point.first.inverse();
    }
}


void inverse(Island<Point<int>> &island) {
    for (point :island.points) 
      point.inverse();
}

//===============================================================================================================


std::vector<Point<int>> generateLine(const Point<int> &p1, const Point<int> &p2) {
  std::vector<Point<int>> points;
  LinearBezier Bezie(p1, p2); 
  for (double i = 0.0; i < 1.0; i+= 0.01) {
    Point<double> point = Bezie.getBezier(i); 
    points.push_back(Point<int>((int)point.x, (int)point.y));
  }

  return points;   
}


std::vector<Island<Point<int>>> generateCurves(std::vector<Island<Point<int>>> &segments) {
  std::vector<Island<Point<int>>> curves;
  for (segment :segments) {
    Island<Point<int>> part;
    for (auto i = segment.points.begin() + 1; i != segment.points.end(); ++i) {  
      Point<int> fst = *(i - 1), snd = *i;
      std::vector<Point<int>> output = generateLine(fst, snd);
      part.points.insert(part.points.end(), output.begin(), output.end());
    }
    curves.push_back(part);
  }
  return curves;
}


void rotateCurves(std::vector<Island<Point<int>>> &curves) {
  // rotation matrix value
  double a, b;
  // here level in Island<Point<int>> is rotation angle
  // and Island<Point<int>>'s centers are rotation centroids 
  for (curve :curves) {
    a = cos(curve.level);
    b = sin(curve.level);
    for (point :curve.points) {
      point.x = (int)(point.x * a + point.y * (-b) + curve.center.x);
      point.y = (int)(point.x * b + point.y * a + curve.center.y);
    }
  }
}


void scaleCurves(std::vector<Island<Point<int>>> &curves, 
  const Point<int> &size) {
  std::pair<double, double> scale((double)size.x / SIZE_X,
                                  (double)size.y / SIZE_Y);
  for (c :curves)
    for (point :c.points) {
      point.x *= scale.first;
      point.y *= scale.second;
    }
}


void shiftCurves(std::vector<Island<Point<int>>> &curves, 
  const Point<int> box[4]) {
  Point<int> dia_r = curves[7].points[0];
  Point<int> dia_l = curves[1].points[0];

  int buff = -1;
  for (point: curves[7].points)
    if (point.y > buff) {
      dia_r = point;
      buff = point.y;
    }

  buff = INT_MAX;
  for (point: curves[1].points)
    if (point.y < buff) {
      dia_l = point;
      buff = point.y;
  }  

  dia_r = Point<int>(box[1].y - dia_r.x - (box[1].y - box[0].y) / 8,
                    box[1].x - dia_r.y);
  dia_l = Point<int>(box[3].y - dia_l.x - (box[3].y - box[2].y) / 8,
                    box[2].x - dia_l.y);

  for (point :curves[7].points) {
    point.x += dia_r.x;
    point.y += dia_r.y;
  }

  for (point :curves[1].points) {
    point.x += dia_l.x;
    point.y += dia_l.y;
  }
}

// inline std::pair<double, double> calculateTensions(const Point<int> &p1, const Point<int> &p2) {
//   // double 
//   // return std::make_pair(,);
// }


//===============================================================================================================


Point<double> *** fieldInit(const Point<int> size) {
  Point<double> ***field = new Point<double>**[NUM_FRAGMENT];
  for (int i = 0; i < NUM_FRAGMENT; ++i) {
      field[i] = new Point<double>*[size.x];
      for (int j = 0; j < size.x; ++j) {
        field[i][j] = new Point<double>[size.y];
        for (int k = 0; k < size.y; ++k) 
          field[i][j][k] = Point<double>(0.0, 0.0);
      }
  }
  return field;
}


void fieldSerrialization(Point<double> ***field, 
  const Point<int> &size) {
  std::ofstream outBinFile; 
  outBinFile.open("FIELD", std::ios::binary);
  for (int i = 0; i < NUM_FRAGMENT; ++i) {
    for (int j = 0; j < size.x; ++j) {
      for (int k = 0; k < size.y; ++k) 
        outBinFile.write ((char*)&field[i][j][k], sizeof(Point<double>));
    }
  }
  outBinFile.close(); 
}


Point<double> *** fieldDeserrialization(const Point<int> &size) {
  Point<double> ***field = fieldInit(size);
  std::ifstream inBinFile; 
  inBinFile.open("FIELD", std::ios::binary);
  for (int i = 0; i < NUM_FRAGMENT; ++i) {
    for (int j = 0; j < size.x; ++j) {
      for (int k = 0; k < size.y; ++k) 
        inBinFile.read ((char*)&field[i][j][k], sizeof(Point<double>));
    }
  }
  return field;
}


Island<Point<int>> matrixToIsland(const Matrix &mask) {
  Island<Point<int>> island;
  assert(mask.rows() == 2);
  for (int i = 0; i < mask.cols(); ++i) {
    Point<int> point((int)mask(0, i), (int)mask(1, i));
    island.points.push_back(point);
  }
  return island;
}


Point<double> *** generateField(std::vector<Island<Point<int>>> &segments, 
  const Point<int> &size, const Point<int> box[4]) {  

  Point<double> *** field = fieldInit(size);
  Matrix mask(size.x, size.y);
  Point<double> bezier_p;
  Point<int> point;

  // rotateCurves(segments);
  printf("Curves are shifted and rotated.\n");
  scaleCurves(segments, size);

  shiftCurves(segments, box);
  // std::vector<Island<Point<int>>> curves = generateCurves(segments);
  BezierApproximation(segments);
  printf("Curves are generated.\n");

  int threshold = (int) (size.x / 8);
  double k = 1.0 / threshold;
  threshold *= threshold;

  int j = 0;
  for (curve :segments) {
    std::cout << "Components of field has been computed: " << j << "\n";
    for (double i = 0; i < 1; i+= 0.001) {
        bezier_p = curve.bezier.getBezier(i);
        point = Point<int>((int)bezier_p.x, (int)bezier_p.y);
         
        bfs(mask, point, 
          [&field, &j, &point, &curve, &k](Point<int> target) { 
              Point<double> rv = point.calcTension(target, k);
              field[j][target.x][target.y].add(rv);
          },
          [&point, &threshold](const Point<int> &target)->bool { 
            return point.getDistanceTo(target) > threshold; 
          }, 
          [&](const double &)->bool { return true; }
        );
    } 
    ++j;
  }
  return field;
}


inline bool checkFeiman(const Matrix &G, const Point<int> &point) {
  double level = G(point.x, point.y);
  if (point.x > 0) {
    if (fabs(G(point.x - 1, point.y) - level) > 0.0001 && G(point.x - 1, point.y) > 0) { 
      return true;
    }
    if (point.y > 0 && fabs(G(point.x - 1, point.y - 1) - level) > 0.0001 && G(point.x - 1, point.y - 1) > 0) {
      return true;
    }
    if (point.y < G.cols() - 1 && fabs(G(point.x - 1, point.y + 1) - level) > 0.0001 && G(point.x - 1, point.y + 1) > 0) {
      return true;
    }
  }
  
  if (point.y > 0 && fabs(G(point.x, point.y - 1) - level) > 0.0001 && G(point.x, point.y - 1) > 0) {
    return true;
  }
  if (point.y < G.cols() - 1 && fabs(G(point.x, point.y + 1) - level) > 0.0001 && G(point.x, point.y + 1) > 0) {
    return true;
  }
  
  if (point.x < G.rows() - 1) {
    if (fabs(G(point.x + 1, point.y) - level) > 0.0001 && G(point.x + 1, point.y) > 0) {
      return true;
    }
    if (point.y < G.cols() - 1 && fabs(G(point.x + 1, point.y + 1) - level) > 0.0001 && G(point.x + 1, point.y + 1) > 0) {
      return true;
    }
    if (point.y > 0 && fabs(G(point.x + 1, point.y - 1) - level) > 0.0001 && G(point.x + 1, point.y - 1) > 0) {
      return true;
    }
  }
  return false;
}


void computeIslands(Matrix &G, const double blevel, 
  std::vector<Island<Point<int>>> &islands) {
  for (int j = 0; j < G.cols(); ++j) {
    for (int i = 0; i < G.rows(); ++i) {
      double level = G(i, j);

      if (fabs(blevel - level) <= 0.0001 || level < -90) 
        continue;
  
      Island<Point<int>> island(level);
      Island<Point<int>> border(level);
      Point<int> point(i, j);


      bfs(G, point, 
        [&island, &border, &G](Point<int> point) { 
          island.points.push_back(point); 
          G(point.x, point.y) = -100; },
        [](const Point<int>&) { return false; },
        [&level](const double &intens) { 
          return fabs(intens - level) <= 0.0001; 
        });


      if (island.points.size() > 100) 
        islands.push_back(island);
    }
  }
}


//===============================================================================================================


Matrix generateMatrix(const std::vector<Island<Point<int>>> &islands, 
  const Point<int> &point) {
  Matrix ret(point.x, point.y);
  ret.fill(0.0);
  std::for_each(islands.begin(), islands.end(), 
    [&](Island<Point<int>> island){
      std::for_each(island.points.begin(), island.points.end(), 
        [&](Point<int> point){
          ret(point.x, point.y) = 0.4;
        });
  });
  return ret;
}


Matrix generateMatrix(const std::vector<Island<std::pair<Point<int>,
 Point<int>>>> &line, 
  const Point<int> &point) {
  Matrix ret(point.x, point.y);
  ret.fill(0.0);
  std::for_each(line.begin(), line.end(), 
    [&](Island<std::pair<Point<int>, Point<int>>> island) {
      std::for_each(island.points.begin(), island.points.end(), 
        [&](std::pair<Point<int>, Point<int>> point) {
          ret(point.first.x, point.first.y) = 0.4;
          ret(point.second.x, point.second.y) = 0.4;
        });
  });
  return ret;
}


Matrix generateMatrix(Point<double> **field, const Point<int> &point) {
  Matrix ret(point.x, point.y);
  ret.fill(0.0);
  for (int i = 0; i < point.x; ++i) 
    for (int j = 0; j < point.y; ++j)
      ret(i, j) = field[i][j].x;
  return ret;
}


Matrix generateMatrixY(Point<double> **field, const Point<int> &point) {
  Matrix ret(point.x, point.y);
  ret.fill(0.0);
  for (int i = 0; i < point.x; ++i) 
    for (int j = 0; j < point.y; ++j)
      ret(i, j) = field[i][j].y;
  return ret;
}


Matrix generateMatrixX(Point<double> **field, const Point<int> &point) {
  Matrix ret(point.x, point.y);
  ret.fill(0.0);
  for (int i = 0; i < point.x; ++i) 
    for (int j = 0; j < point.y; ++j)
      ret(i, j) = field[i][j].y;
  return ret;
}


Matrix generateBezierMatrix(const std::vector<Island<Point<int>>> &line, 
  Matrix &ret) {
  std::for_each(line.begin(), line.end(), 
    [&](Island<Point<int>> island) {
      for (double i = 0; i < 1; i+= 0.001) {
        Point<double> Bezie = island.bezier.getBezier(i);
        ret((int)Bezie.x, (int)Bezie.y) = 1;
      }
  });
  return ret;
}


Matrix generateBezierMatrix(const std::vector<Island<Point<int>>> &line, 
  const Point<int> &point) {
  Matrix ret(point.x, point.y);
  ret.fill(0.0);
  return generateBezierMatrix(line, ret);
}


Matrix generateBezierDeravativeMatrix(const std::vector<Island<std::pair<Point<int>,
 Point<int>>>> &line, 
  Matrix &ret) {
  std::for_each(line.begin(), line.end(), 
    [&](Island<std::pair<Point<int>, Point<int>>> island) {
      for (double i = 0; i < 1; i+= 0.001) {
        Point<double> Bezie = island.BezierPairs.first.getBezierDeravative(i);
        if (Bezie.x > 0 && Bezie.y > 0 && Bezie.x < ret.rows() && Bezie.y < ret.cols())
        ret((int)Bezie.x, (int)Bezie.y) = 1;
        Bezie = island.BezierPairs.second.getBezierDeravative(i);
        if (Bezie.x > 0 && Bezie.y > 0 && Bezie.x < ret.rows() && Bezie.y < ret.cols())
        ret((int)Bezie.x, (int)Bezie.y) = 1;
      }
  });
  return ret;
}


Matrix generateBezierDeravativeMatrix(const std::vector<Island<std::pair<Point<int>,
 Point<int>>>> &line, 
  const Point<int> &point) {
  Matrix ret(point.x, point.y);
  ret.fill(0.0);
  return generateBezierDeravativeMatrix(line, ret);
}

Matrix landmarcsAsMatrix(Island<Point<int>> &LM) {
  Matrix ret(LM.points.size(),2);
  for (int i = 0; i < LM.points.size(); i++) {
    ret(i, 0) = LM.points[i].x;
    ret(i, 1) = LM.points[i].y;
  }
  return ret;
}


//===============================================================================================================


std::pair<Point<double>, Point<double>> centers(const Point<int> &size) {
  std::pair<Point<double>, Point<double>> 
          center(Point<double>(856.0, 856.0),
                 Point<double>(856.0, 1714.0));


  Point<double> scale((double)size.x / SIZE_X,
                                  (double)size.y / SIZE_Y);
  center.first  *= scale;
  center.second *= scale;
  // double size = 0.0;
 
  // for (int i = 1; i < 6; ++i) {
  //   for (auto l = components[i].begin(); 
  //     l != components[i].end()
  //     && l != components[i].begin() + 1; ++l)
  //     center.first += l->bezier.Q0;
  //   size += components[i].size();
  // }
  // center.first /= size;
  // size = 0.0;

  // for (int i = 6; i < NUM_FRAGMENT; ++i) {
  //   for (auto l = components[i].begin(); 
  //     l != components[i].end()
  //     && l != components[i].begin() + 1; ++l)
  //     center.second += l->bezier.Q0;
  //   size += components[i].size();
  // }
  // center.second /= size;
  return center;
}


// Point<double> calcAngle(const Island<Point<int>> island, 
//                         const Point<int> &center) {
  
//   Point<double> angle(island.bezier.Q0.y - center.y,
//                       island.bezier.Q3.y - center.y);
  
//   double dx = island.bezier.Q0.x - center.x;
//   angle.x = dx ? atan(angle.x / dx) : angle.x > 0? M_PI / 2 : -M_PI / 2;
//   dx = island.bezier.Q3.x - center.x;
//   angle.y = dx ? atan(angle.y / dx) : angle.y > 0? M_PI / 2 : -M_PI / 2;

//   return angle;
// }


void adapticThreshold(std::vector<Island<Point<int>>> *components) {
  double thresholds[NUM_FRAGMENT] = {0.0};

  for (int i = 0; i < NUM_FRAGMENT; ++i) 
    std::sort(components[i].begin(), components[i].end(), 
      [&i](const Island<Point<int>> &lft, 
           const Island<Point<int>> &rgh) -> bool {
        return lft.agregator.components[i] 
             > rgh.agregator.components[i];
    }); 

  for (int i = 0; i < NUM_FRAGMENT; ++i) {
    double maxDist = 0.0;
    for (auto j = components[i].begin(); 
         j != components[i].end() && j != components[i].end() - 1;) {
      double dist = j->agregator.components[i] - (++j)->agregator.components[i];
      if (dist > maxDist && j->agregator.components[i] > 0) {
        maxDist = dist;
        thresholds[i] = j->agregator.components[i];
      }
    }
  }

  for (int i = 0; i < NUM_FRAGMENT; ++i) {

    for (auto j = components[i].begin(); j != components[i].end();) 
      if (j->agregator.components[i] >= thresholds[i])
        ++j;
      else
        j = components[i].erase(j);
  }
}      


void angleClassifier(std::vector<Island<Point<int>>> *components, 
  const Point<int> &size) {

  std::pair<Point<double>, Point<double>> lungCenters = centers(size);
  
  std::cout << "centers:  fst.x = " << lungCenters.first.x << " fst.y = " 
    << lungCenters.first.y << " snd.x = " << lungCenters.second.x
    << " snd.y = " << lungCenters.second.y << "\n";

  // RIGHT LUNG
  for (int i = 0; i < 6; ++i) 
    if (components[i].size() > 1)
      for (auto p = components[i].begin(); p != components[i].end() - 1; ++p) {
        for (auto q = p + 1; q != components[i].end();) {     
          
          if (isCovered(*q, *p, lungCenters.first)) {
            if (q->agregator.components[i] < p->agregator.components[i]) {
              q = components[i].erase(q);
              continue;
            }
            else {
              p = components[i].erase(p);
              break;
            }
          }
          ++q;
        }
        if (p == components[i].end() - 1 || p == components[i].end())
          break;
      }

  //RIGHT LUNG
  for (int i = 6; i < NUM_FRAGMENT; ++i) 
    if (components[i].size() > 1)
      for (auto p = components[i].begin(); p != components[i].end() - 1; ++p) {
        for (auto q = p + 1; q != components[i].end();) {     
          
          if (isCovered(*q, *p, lungCenters.second)) {
            if (q->agregator.components[i] < p->agregator.components[i]) {
              q = components[i].erase(q);
              continue;
            }
            else {
              p = components[i].erase(p);
              break;
            }
          }
          ++q;
        }
        if (p == components[i].end() - 1 || p == components[i].end())
          break;
      }
}


void sortByAngle(std::vector<Island<Point<int>>> *components, 
  const Point<double> &center,
  const int begin = 0,
  const int end = NUM_FRAGMENT) {
  for (int i = begin; i < end; ++i) {
    std::sort(components[i].begin(), components[i].end(), 
      [&center](const Island<Point<int>> &lft, 
           const Island<Point<int>> &rgh) -> bool {
        return angleComparator(rgh, lft, center);
    }); 
  }
}

std::pair<Point<double>, Point<double>> minDistPoints(const Point<double> Q0, 
  const Point<double> Q1, 
  const Point<double> P0, 
  const Point<double> P1) {

  std::pair<Point<double>, Point<double>> pair(Q0, P0);
  double minDist = Q0.getDistanceTo(P0);
  
  double dist = Q0.getDistanceTo(P1);  
  if (dist < minDist) {
    minDist = dist;
    pair.second = P1;
  }

  dist = Q1.getDistanceTo(P0);  
  if (dist < minDist) {
    minDist = dist;
    pair.first  = Q1;
    pair.second = P0;
  }  

  dist = Q1.getDistanceTo(P1);  
  if (dist < minDist) {
    minDist = dist;
    pair.first  = Q1;
    pair.second = P1;
  }  

  return pair;
}


std::pair<Point<double>, Point<double>> minAnglePoints(
  const Point<double> &Q0, 
  const Point<double> &Q1, 
  const Point<double> &P0, 
  const Point<double> &P1,
  const Point<double> &center) {

  std::pair<Point<double>, Point<double>> pair(Q0, P0);
  
  if (!angleComparator(Q0, Q1, center))
    pair.first = Q1;

  if (angleComparator(P0, P1, center))
    pair.second = P1;

  return pair;
}


Point<double> lowPoint(const Island<Point<int>> &island) {
  return island.bezier.Q0.x < island.bezier.Q3.x ?
               island.bezier.Q0 : island.bezier.Q3;
}


Point<double> heighPoint(const Island<Point<int>> &island) {
  return island.bezier.Q0.x > island.bezier.Q3.x ?
               island.bezier.Q0 : island.bezier.Q3;
}


Island<Point<int>> uniteLines(const Island<Point<int>> &rgh, 
                              const Island<Point<int>> &lft,
                              const Point<double> &center,
                              const int step) {

  std::pair<Point<double>, Point<double>> pair;
  pair = minAnglePoints(rgh.bezier.Q0, rgh.bezier.Q3, 
                        lft.bezier.Q0, lft.bezier.Q3,
                        center);  
  switch (step) {
    case 1:
      pair.second = heighPoint(lft);
      break;
    case 4:
      pair.first = lowPoint(rgh);
      break;
    case 5:
      pair.first = lowPoint(rgh);
      pair.second = heighPoint(lft);
      break;    
  }

  Island<Point<int>> mean(0.0);
  mean.bezier = Bezier(pair.first, pair.first, 
                       pair.second, pair.second);

  return mean;
}


Island<Point<int>> uniteLines(const Island<Point<int>> &rgh, 
                              const Island<Point<int>> &lft) {

  std::pair<Point<double>, Point<double>> 
        pair = minDistPoints(rgh.bezier.Q0, rgh.bezier.Q3, 
                             lft.bezier.Q0, lft.bezier.Q3);  

  Island<Point<int>> mean(0.0);
  mean.bezier = Bezier(pair.first, pair.first, 
                       pair.second, pair.second);

  return mean;
}


void fillByLines(const std::vector<Island<Point<int>>> *components,
                 std::vector<Island<Point<int>>> &final,
                 const std::pair<Point<double>, Point<double>> &center) {

  for (int i = 0; i < NUM_FRAGMENT; ++i) {
    std::cout << "Component " << i << ": ";
    for (auto j = components[i].begin(); j != components[i].end(); ++j) { 
        
        if ((j+1) != components[i].end()) {
          if (i < 6)
            final.push_back(uniteLines(*j, *(j+1)));
          else
            final.push_back(uniteLines(*j, *(j+1)));
        } else {
          if (i && components[i - 1].size()) {
            if (i < 6)
              final.push_back(uniteLines(*(components[i].begin()), 
                                       *(components[i - 1].begin()), 
                                        center.first, i));
            else if (i > 6)
              final.push_back(uniteLines(*(components[i].begin()), 
                                       *(components[i - 1].begin()), 
                                        center.second, i));
          }
        }

        std::cout << j->agregator.components[i] << " ";
        final.push_back(*j);
      }
    std::cout << "\n";
  }
}


std::vector<Island<Point<int>>> classificator(const std::vector<Island<Point<int>>> &lines,
  std::vector<Island<Point<int>>> components[NUM_FRAGMENT],
  const Point<int> &size) {

  std::pair<Point<double>, Point<double>> lungCenters = centers(size);
  std::vector<Island<Point<int>>> final;

  for (l :lines) {
    int i = l.agregator.max();

    if (i >= 0)
      components[i].push_back(l);
  } 

  std::cout << "Start angle classification... " << components[0].size() << "\n";
  adapticThreshold(components);  
  angleClassifier(components, size);
  std::cout << "Start sorting by angle... "  << components[0].size() << "\n";
  sortByAngle(components, lungCenters.first, 0, 6);
  sortByAngle(components, lungCenters.second, 6, NUM_FRAGMENT);

  fillByLines(components, final, lungCenters);


  final.push_back(generateShape(Point<int>((int)lungCenters.first.x, 
                  (int)lungCenters.first.y)));
  // final.push_back(generateShape(Point<int>((int)lungCenters.second.x, 
  //                 (int)lungCenters.second.y)));  
 
  return final;

  // for (int i = 0; i < NUM_FRAGMENT; ++i) {
  //   std::sort(components[i].begin(), components[i].end(), 
  //     [&i](const Island<std::pair<Point<int>, Point<int>>> &lft, 
  //          const Island<std::pair<Point<int>, Point<int>>> &rgh) -> bool {
  //       return lft.agregatorPairs.first.components[i] 
  //             < rgh.agregatorPairs.first.components[i];
  //   });

  // }
}


void agregate(std::vector<Island<Point<int>>> &lines, Point<double> ***field) {
  for (l :lines) { 
    double step = 1.0 / sqrt(l.bezier.Q0.getDistanceTo(l.bezier.Q3));
    for (double j = 0; j <= 1; j+= step) {
      Point<double> B(l.bezier.getBezier(j));

      Point<double> BDer(normalized(l.bezier.getBezierDeravative(j)));

      for (int i = 0; i < NUM_FRAGMENT; ++i) {
        double inc = fabs(BDer * field[i][(int) B.x][(int) B.y]);
        l.agregator.inc(i, inc ? inc : -0.01);
      }
    }
  }
}



// void agregate(line &lines, Point<double> ***field) {
//   for (l :lines) {
//     for (double i = 0; i < 1; i+= 0.05) {
//         std::pair<Point<double>, Point<double>> 
//             B(l.BezierPairs.first.getBezier(i), 
//               l.BezierPairs.second.getBezier(i));

//         std::pair<Point<double>, Point<double>> 
//             BDer(normalized(l.BezierPairs.first.getBezierDeravative(i)),
//                  normalized(l.BezierPairs.second.getBezierDeravative(i)));

//         for (int i = 0; i < NUM_FRAGMENT; ++i) {
//           l.agregatorPairs.first.inc(i, 
//                 abs(BDer.first * field[i][(int) B.first.x][(int) B.first.y]));

//           l.agregatorPairs.second.inc(i, 
//                 abs(BDer.second * field[i][(int) B.second.x][(int) B.second.y]));
//         }
//       }
//   }
// }


//===============================================================================================================


Matrix generateEnergy(const Matrix &G) {
  //Energy Matrix 
  Matrix E(G.rows(), G.cols());
  //Matrix of transition vectors in (-1, 0, 1) 
  Matrix V(G.rows(), G.cols());

  std::vector<Island<Point<int>>> seams;
  Point<int> point;

  for (int i = 0; i < G.cols(); ++i) 
    E(0, i) = G(0, i);

  for (int i = 1; i < E.rows(); ++i)
    for (int j = 1; j < E.cols() -1; ++j)
      if (E(i - 1, j) <= E(i - 1, j - 1)) {
        if (E(i - 1, j) <= E(i - 1, j + 1)) {
          E(i, j) = G(i, j) + E(i - 1, j); 
          V(i, j) = 0;
        } else {
          E(i, j) = G(i, j) + E(i - 1, j + 1); 
          V(i, j) = 1;
        }
      } else {
        if (E(i - 1, j - 1) <= E(i - 1, j + 1)) {
          E(i, j) = G(i, j) + E(i - 1, j - 1); 
          V(i, j) = -1;
        } else {
          E(i, j) = G(i, j) + E(i - 1, j + 1); 
          V(i, j) = 1;
        }
      }


  for (int i = 0; i < E.rows(); ++i)
    for (int j = 0; j < E.cols(); ++j)
      E(i, j) = E(i, j) / ((double) E.rows() + 1); 

  // for (int i = 0; i < E.cols(); ++i) 
  //   seams.push_back(Island<Point<int>>(E(E.rows(), i), E.rows(), i));

  // auto trace = [](const Island<Point<int>> &lft, const Island<Point<int>> &rgh) -> bool {
  //     return lft.level < rgh.level;
  //   };


  // std::nth_element(seams.begin(), 
  //   seams.begin() + size, seams.end(), trace);

  // seams.resize(size);

  // std::sort(seams.begin(), seams.end(), trace);


  // for (seam :seams) {
  //   G(seam.center.x, seam.center.y) = -1;
  //   for (int i = G.rows(); i >= 0; --i) {
      
  //   }
  // }

  
  return E;
}


inline double applyFilter(Matrix &I, const Matrix &filter, 
  const Point<int> &point) {
  int radius = (filter.rows() - 1)/ 2;
  double accum = 0;
  for (int i = -radius; i < radius; ++i)
    for (int j = -radius; j < radius; ++j) 
      if (point.x + i < I.rows() && point.y + j < I.cols()
         && point.x + i >= 0 && point.y + j >= 0)
        accum += I(point.x + i, point.y + j) 
          * filter(i + radius, j + radius);   
  return accum;
}


Matrix generateGaussian(double sigma, int radius = 0) {
  double s = 2 * SQR(sigma);
  double a = 1/(s * M_PI);
  if (!radius)
    radius = (int)round(sigma * 3);
  Matrix filter(2 * radius + 1, 2 * radius + 1);
  for (int i = -radius; i <= radius; ++i)
    for (int j = -radius; j <= radius; ++j)
      filter(i + radius, j + radius) = a * exp(-(SQR(i)/s + SQR(j)/s));
  return filter;
}



void applyGaussian(Matrix &I, const std::vector<Island<Point<int>>> borders) {
  Matrix filter = generateGaussian(5.0);
  std::cout << "Gaussian are generated\n";
  std::cout << filter.rows() << "\n";
  for (border :borders) {
    std::vector<double> buff;
    for (point :border.points)
      buff.push_back(applyFilter(I, filter, point));
    int i = 0;
    for (point :border.points)
        I(point.x, point.y) = buff.at(i++);
    buff.clear();
  }
}


//===============================================================================================================

void applyLeftCorrection(Island<Point<int>> &LM, 
  const Island<Point<int>> &component) {

  inverse(LM);

  std::pair<Point<double>, Point<double>> fronts 
      = std::make_pair(component.bezier.getBezier(0), 
                       component.bezier.getBezier(1));

  if (fronts.first.y > fronts.second.y)
    std::swap(fronts.first, fronts.second);

  //IF bottom right is good (best case)
  if (LM.points[2].y < fronts.second.y) {
    LM.points[2] = Point<int>((int)fronts.second.x, (int)fronts.second.y);

    if (LM.points[3].y < fronts.second.y)
      LM.points[3] = LM.points[2];

    int buff = INT_MAX;
    for (point: component.points)
      if (point.y == LM.points[1].y
          && point.x < buff) {
        LM.points[1] = point;
        buff = point.x;
      }

    return;
  }
  //===================================
  
  //Find shift vector
  int buff = INT_MAX;
  Point<int> target;
  for (point: component.points)
    if (point.y == LM.points[0].y && point.x < buff) {
      target = point;
      buff = point.x;
    }

  //In warste case nothing to do
  if (buff == INT_MAX)
    return;
  
  int shift = target.x - LM.points[0].x;
  for (int i = 0; i < 3; i++)
    LM.points[i].x += shift; 

  if (LM.points[3].y < LM.points[2].y)
      LM.points[3] = LM.points[2];

  std::vector<int> diff;
  for (point: component.points)
    if (point.y == LM.points[1].y)
      diff.push_back(point.x);
  
  if (diff.size()) {
    int mean = 0;
    std::pair<int, int> max = std::make_pair(INT_MAX, 0);
    for (d: diff) {
      mean += d;
      if (d > max.second)
        max.second = d;
      if (d < max.first)
        max.first = d;
    }
    mean /= diff.size();
    mean -= (max.second - max.first) / 4;
    LM.points[1].x = mean;
  }
}


void applyRightCorrection(Island<Point<int>> &LM, 
  const Island<Point<int>> &component) {

  //DO NOT FORGET ABOUT INVERSE
  inverse(LM);

  std::pair<Point<double>, Point<double>> fronts 
      = std::make_pair(component.bezier.getBezier(0), 
                       component.bezier.getBezier(1));

  std::cout << component.points.size() << " 1\n";
  if (fronts.first.y < fronts.second.y)
    std::swap(fronts.first, fronts.second);

  
  //IF bottom right is good (best case)
  printf("%d %d\n %f %f\n %f %f\n", LM.points[2].x,LM.points[2].y,fronts.second.x,fronts.second.y,fronts.first.x,fronts.first.y);
  if (LM.points[0].y > fronts.second.y) {
    std::cout << "2\n";
    LM.points[0] = Point<int>((int)fronts.second.x, (int)fronts.second.y);

    if (LM.points[3].y > fronts.second.y)
      LM.points[3] = LM.points[0];

    int buff = INT_MAX;
    for (point: component.points)
      if (point.y == LM.points[1].y
          && point.x < buff) {
        LM.points[1] = point;
        buff = point.x;
      }

    return;
  }
  //===================================
  
  std::cout << "3\n";
  //Find shift vector
  int buff = INT_MAX;
  Point<int> target;
  for (point: component.points)
    if (point.y == LM.points[2].y && point.x < buff) {
      target = point;
      buff = point.x;
    }

  //In warste case nothing to do
  if (buff == INT_MAX)
    return;

  std::cout << "4\n";
  
  int shift = target.x - LM.points[2].x;
  std::cout << shift << "\n";
  for (int i = 0; i < 3; i++)
    LM.points[i].x += shift; 

  if (LM.points[3].y > LM.points[0].y)
      LM.points[3] = LM.points[0];

  std::vector<int> diff;
  for (point: component.points)
    if (point.y == LM.points[1].y)
      diff.push_back(point.x);
  
  if (diff.size()) {
    int mean = 0;
    std::pair<int, int> max = std::make_pair(INT_MAX, 0);
    for (d: diff) {
      mean += d;
      if (d > max.second)
        max.second = d;
      if (d < max.first)
        max.first = d;
    }
    mean /= diff.size();
    mean -= (max.second - max.first) / 4;
    LM.points[1].x = mean;
  }
}

//===============================================================================================================


DEFUN_DLD(lungDICOM, args, nargout, "DICOM") {
  octave_value_list retval;
  srand(time(0));
  Matrix I;
  Matrix G[4];
  Matrix curves[NUM_FRAGMENT];
  double blevel[4];
  std::vector<Island<Point<int>>> components[NUM_FRAGMENT];
  std::vector<Island<Point<int>>> linesZip;
  std::vector<Island<Point<int>>> final;
  line lines[4];
  std::vector<Island<Point<int>>> islands[4];
  std::vector<Island<Point<int>>> borders[4];
  std::vector<Island<Point<int>>> ethalon;
  Point<int> box[4];
  
  std::cout << "Start...\n";

  I = args(0).matrix_value();


  for (int i = 0; i < 4; ++i) 
    G[i] = args(i + 1).matrix_value();

  for (int i = 0; i < 4; ++i) {
    blevel[i] = args(i + 5).array_value()(0); 
    printf("BLEVEL = %f\n", blevel[i]);
  }

  for (int i = 0; i < 4; ++i) {
    int x = args(i + 9).array_value()(0);
    int y = args(i + 13).array_value()(0);
    box[i] = Point<int>(x, y);
    printf("BOX: x = %d, y = %d\n", x, y);
  }

  Matrix LM[2] = {args(17).matrix_value().transpose(),
                  args(18).matrix_value().transpose()};

  Island<Point<int>> LM_l = matrixToIsland(LM[0]);
  Island<Point<int>> LM_r = matrixToIsland(LM[1]);


  JSON parsed(args, 19);
  Point<int> size(G[0].rows(), G[0].cols());
  printf("SIZE: x = %d, y = %d\n", size.x, size.y);
  std::cout << "JSON have been parsed.\n";  


  //TODO: REWRITE IN TEMPLATE 
  // Point<double> ***field = fieldInit(size);


  std::cout << "Start aproximating ethalon...\n";
  for (int i = 0; i < NUM_FRAGMENT; ++i) {
    Island<Point<int>> island = matrixToIsland(parsed.Contours[i]);
    ethalon.push_back(island);
  }

  Point<double> *** field = generateField(ethalon, size, box); 
  // fieldSerrialization(field, size);


  std::cout << "Ethalon has been aproximated.\n";


  std::cout << "Data has been readed.\n";
  // std::cout << "Start computing islands...\n";
  
  for (int i = 0; i < 4; ++i) {
    computeIslands(G[i], blevel[i], islands[i]);
    std::cout << "Computing island: " << i << "\n";
  }
  
  std::cout << "Islands have been computed.\n ---------------------------------------\n";

  /*
   * Test for splitByLine()
   */

  for (int i = 0; i < 2; ++i) { 
    // if (!i)
    //   inverse(islands[i]);
    for (island :islands[i]) {
      line l =  splitByLines(island.points);
      // if (!i)
      //   inverse(l);
      std::vector<Island<Point<int>>> zip_l = zip(l);
      BezierApproximation(zip_l);
      linesZip.insert(linesZip.end(), zip_l.begin(), zip_l.end());
    }
  }

  std::cout << "Start agregating islands...\n";
  agregate(linesZip, field);
  std::cout << "Start classificating islands...\n";
  final = classificator(linesZip, components, size);

  applyLeftCorrection(LM_l, components[1][0]);
  applyRightCorrection(LM_r, components[7][0]);
  LM[0] = landmarcsAsMatrix(LM_l);
  LM[1] = landmarcsAsMatrix(LM_r);
  // std::cout << "HERE\n";
  Matrix pre = generateMatrix(linesZip, Point<int>(G[0].rows(), G[0].cols()));
  retval(0) = generateBezierMatrix(linesZip, pre);
  // applyGaussian(I, islands[1]);

  pre = generateMatrix(final, Point<int>(G[0].rows(), G[0].cols()));
  retval(1) = generateBezierMatrix(final, pre);
  
  // std::cout << "Start computing energy...\n";
  // retval(0) = generateEnergy(I, 100);
  // std::cout << "Energy has been computed.\n";


  // std::cout << "generated...\n";
  // for (int i = 0; i < 4; ++i)
  //   retval(i + 1) = generateMatrix(islands[i], Point<int>(G[i].rows(), G[i].cols())); 


  std::cout << "generated...\n";
  // for (int i = 0; i < NUM_FRAGMENT; ++i) {
  pre = generateMatrix(components[1], size);
  retval(2) =  generateBezierMatrix(components[1], pre);
  pre = generateMatrix(components[7], size);
  retval(3) =  generateBezierMatrix(components[7], pre);
  retval(4) = LM[0];
  retval(5) = LM[1];
  // }
  


  // std::cout << "generated...\n";
  // for (int i = 0; i < NUM_FRAGMENT; ++i) 
  //   retval(i + NUM_FRAGMENT + 2) =  generateMatrixX(field[i], size);
  
  

  return retval;
}
