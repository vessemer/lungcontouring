# README #

Detecting lung's contours on DICOM image. Problem is related to gas accumulation in stomach, which forms a bubble with the same density a lung has, also it has similar texture to lung so it is hard to determine the difference. The idea is to find a diaphragm contour separating lung and stomach and then get whole boundaries.