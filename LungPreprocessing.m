## Copyright (C) 2016 vess
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} Lung (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: vess <vess@localhost>
## Created: 2016-01-25

function [A, derivative, threshold, box_X, box_Y, JSON] = Lung(pathologies, number, xy)
%
% Detailed explanation goes here
% pathologies = 'Infiltration';
% for number = 1:7
    pkg load image;
    pkg load octclip;
    addpath("/home/vess/octave/jsonlab");

    path = strcat('../data/', pathologies, '/image', int2str(number), '.');
    box = load(strcat('../data/', pathologies, '/', int2str(number), '_bor.mat'));
    ASM = load(strcat('../data/', pathologies, '/', int2str(number), '.mat'));
    info = dicominfo(strcat(path, 'dcm'));
    

    % data = loadjson(strcat(path, 'json'));
    data = loadjson('../data/Fibrosis/image1.json');
    JSON = {data.LeftLungContour.XHeartInner data.LeftLungContour.YHeartInner data.LeftLungContour.XDiafragm data.LeftLungContour.YDiafragm data.LeftLungContour.XOuter data.LeftLungContour.YOuter data.LeftLungContour.XTop data.LeftLungContour.YTop data.LeftLungContour.XTopInner data.LeftLungContour.YTopInner data.LeftLungContour.XHeartRoot data.LeftLungContour.YHeartRoot data.RightLungContour.XOuter data.RightLungContour.YOuter data.RightLungContour.XDiafragm data.RightLungContour.YDiafragm data.RightLungContour.XInner data.RightLungContour.YInner data.RightLungContour.XTop data.RightLungContour.YTop};% data.Clavicle.XRight data.Clavicle.YRight data.Clavicle.XLeft data.Clavicle.YLeft};
    %shape = strel('disk', 10);
    
 
    %shape = strel('disk', 10);
    I = dicomread(info);
    % imshow(I);
    % GX = I;

    % figure
    % subplot(1,2,1);
    % imshow(I);
    %I = imresize(I, [ceil(size(I, 1) * 0.4), ceil(size(I, 2) * 0.4)]);
    % subplot(1,2,2);
    % new = [size(I, 1) - 10, size(I, 2) - 10];
    % I(:,:,2) = I(:,:,1);
    % I(:,:,3) = I(:,:,1);
    % J = Seam(new, I);


    hy = fspecial('gaussian', 5, 2);
    I = imfilter(I, hy, 'replicate');
    I = mat2gray(I);
    I = histeq(I);
    % imshow(I);
    % hold on
    %hold on
    % title(strcat(pathologies, int2str(number)));
    % if strcmp(pathologies, 'Norm')
        % plot(data.LeftLungContour.XHeartInner, data.LeftLungContour.YHeartInner, data.LeftLungContour.XDiafragm, data.LeftLungContour.YDiafragm, data.LeftLungContour.XOuter, data.LeftLungContour.YOuter, data.LeftLungContour.XTop, data.LeftLungContour.YTop, data.LeftLungContour.XTopInner, data.LeftLungContour.YTopInner, data.LeftLungContour.XHeartRoot, data.LeftLungContour.YHeartRoot, data.RightLungContour.XOuter, data.RightLungContour.YOuter, data.RightLungContour.XDiafragm, data.RightLungContour.YDiafragm, data.RightLungContour.XTop, data.RightLungContour.YTop, data.RightLungContour.XInner, data.RightLungContour.YInner, 'LineWidth',4)
        % plot(data.LeftLungContour.XDiafragm, data.LeftLungContour.YDiafragm, data.RightLungContour.XDiafragm, data.RightLungContour.YDiafragm, data.Clavicle.XRight, data.Clavicle.YRight, data.Clavicle.XLeft, data.Clavicle.YLeft, 'LineWidth',4)
    % else
        % plot(data.LeftLungContour.XHeartInner , data.LeftLungContour.YHeartInner, 'LineWidth',4, data.LeftLungContour.XDiafragm, data.LeftLungContour.YDiafragm, 'LineWidth',4, data.LeftLungContour.XOuter, data.LeftLungContour.YOuter, 'LineWidth',4, data.LeftLungContour.XTop, data.LeftLungContour.YTop, 'LineWidth',4, data.LeftLungContour.XTopInner, data.LeftLungContour.YTopInner, 'LineWidth',4, data.LeftLungContour.XHeartRoot, data.LeftLungContour.YHeartRoot, 'LineWidth',4, data.RightLungContour.XOuter, data.RightLungContour.YOuter, 'LineWidth',4, data.RightLungContour.XDiafragm, data.RightLungContour.YDiafragm, 'LineWidth',4, data.RightLungContour.XTop, data.RightLungContour.YTop, 'LineWidth',4, data.RightLungContour.XInner, 'LineWidth',4, data.RightLungContour.YInner, 'LineWidth',4, data.Pathologies{1,1}.Contour.XCoordinates, data.Pathologies{1,1}.Contour.YCoordinates, 'LineWidth',4)
    % end

    %I = image_normalization2(I, J, 5);
    %I = histeq(I);
    % subplot(1,2,2);
    % imshow(I);



    %contour(I, 3);
    %subplot(1,5,1);
    % imshow(I);

    %gb = gabor_fn(6.8, 5, 1.2, 1.5, 1.8, 0.6);

    %GB = imfilter(I, gb, 'replicate', 'conv');



    k = 7;
    t = 3.8;
    l = 5.4;


    % plot([a.base_points{1}(:,2) / 5.4; a.base_points{1}(1,2) / 5.4], [a.base_points{1}(:,2) / 5.4; a.base_points{1}(1,1) / 5.4] 'g')
    box_X = {box.x{1}(1)/l, box.x{1}(2)/l, box.x{1}(3)/l, box.x{1}(4)/l};
    box_Y = {box.y{1}(1)/l, box.y{1}(2)/l, box.y{1}(3)/l, box.y{1}(4)/l};
    % % for  l = [1.8, 2.4]
        J = imresize(I, 1.0 / l);



    data = loadjson(strcat(path, 'json'));
    Left = [data.LeftLungContour.XCoordinates', data.LeftLungContour.YCoordinates']/l;
    Right = [data.RightLungContour.XCoordinates', data.RightLungContour.YCoordinates']/l;


    ASM_l = [(ASM.base_points{1}(:,2) / l), (ASM.base_points{1}(:,1) / l)];
    ASM_r = [(ASM.base_points{2}(:,2) / l), (ASM.base_points{2}(:,1) / l)];
    

    BP_l = [ASM.base_points{1}(ASM.LM{1},2) / l, ASM.base_points{1}(ASM.LM{1},1) / l];
    BP_r = [ASM.base_points{2}(ASM.LM{2},2) / l,ASM.base_points{2}(ASM.LM{2},1) / l];
    
    
    LM_L = ones(1, 16);
    j=1;
    for i=1:size(ASM.LM{1},2)
        if ASM.LM{1}(i) == 1
            LM_L(j) = i;
            ++j;
        end
    end

    LM_R = ones(1, 12);
    j=1;
    for i=1:size(ASM.LM{2},2)
        if ASM.LM{2}(i) == 1
            LM_R(j) = i;
            ++j;
        end
    end

    BP_l = BP_l([1:3 5],:)
    BP_r = BP_r([1:3 12],:)
    BP = {BP_l BP_r};

    %     % BEST T == 3.2, 3, 5 

    %     % for t = [3, 3.2, 5]
            % [C, h] = contour(J, 6);
            % figure 
            % clabel(C, h, 6, 'fontsize', 1);
            hy = fspecial('gaussian', 6, 1.1);
            J = mat2gray(imfilter(J, hy, 'replicate'));

            [GX,GY,GR,GL] = gaussgradient(J, t);

            GX = histeq(double(mat2gray(GX)), k);

            GY = histeq(double(mat2gray(GY)), k);

            GR = histeq(double(mat2gray(GR)), k);

            GL = histeq(double(mat2gray(GL)), k);

            GX = GY;
            k = 0.5;
            R = 128;
            n = 5;
            filtered = colfilt(GX, [n n], "sliding", @(x) (mean(x).*(1+0.5*(std(x)/128 - 1))));
            GX(GX < filtered) = 0;
            GX(GX >= filtered) = 255;
            shape = strel('disk', 10, 0);
            GX = imopen(GX, shape);
            shape = strel('disk', 5, 0);
            GX = imclose(GX, shape);

            % figure
            % subplot(1,4,1)
            % imshow(GX)
            % subplot(1,4,2)
            % imshow(GY)
            % subplot(1,4,3)
            % imshow(GR)
            % subplot(1,4,4)
            % imshow(GL)

            J = histeq(J);

            A = J;%edge(mat2gray(J), 'canny', 0.02);
            % can = histeq(double(A), k);
            % shape = strel('disk', 20, 0);
            % can = imclose(can, shape);


            a = mode(mode(GX))
            b = mode(mode(GY))
            c = mode(mode(GR))
            d = mode(mode(GL))

            derivative = {GX, GY, GR, GL};

            threshold = {a, b, c, d};

    
            % a = 0;
            % GX = A;

            % imwrite (E, strcat('./', pathologies, '/TEST-NUM', num2str(number),'l', num2str(l), '-t', num2str(t), '-k', num2str(k), '.png'));
        % end
    % end

    [A, E, e2, e8, LM_LN, LM_RN] = lungDICOM(J, derivative{:}, threshold{:}, box_X{:}, box_Y{:}, BP{:}, JSON{:});


    figure
    imshow(J + e8 + e2);
    hold on;

    BP_l = [LM_LN(:,2), LM_LN(:,1)];
    BP_r = [LM_RN(:,2), LM_RN(:,1)];

    T = [ASM.base_points{1}(LM_L(6):end,2) / l, ASM.base_points{1}(LM_L(6):end,1) / l];
    NEW_l = [BP_l;T];
    NEW_l = [NEW_l; NEW_l(1,:)];
    
    T = [ASM.base_points{2}(LM_L(4):LM_L(11),2) / l, ASM.base_points{2}(LM_L(4):LM_L(11),1) / l];
    NEW_r = [BP_r(1:3,:);T;BP_r(4,:)];
    NEW_r = [NEW_r; NEW_r(1,:)];

    plot(NEW_l(:,1), NEW_l(:,2), 'b');
    plot(NEW_r(:,1), NEW_r(:,2), 'b');

    L = [ASM.base_points{1}(:,2) / l, ASM.base_points{1}(:,1) / l];
    L = [L;L(1,:)];
    plot(L(:,1), L(:,2), 'r');
    R = [ASM.base_points{2}(:,2) / l, ASM.base_points{2}(:,1) / l];
    R = [R;R(1,:)];
    plot(R(:,1), R(:,2), 'r');
    
    scatter(BP_l(:,1), BP_l(:,2) ,10, 'filled');
    scatter(BP_r(:,1), BP_r(:,2) ,10, 'filled');

    [X_r,Y_r,nP] = oc_polybool(Right, R, 'AND');
    [X_l,Y_l,nP] = oc_polybool(Left, L, 'AND');
    Inter = polyarea(X_l, Y_l) + polyarea(X_r, Y_r)

    [X_r,Y_r,nP] = oc_polybool(Right, R, 'OR');
    [X_l,Y_l,nP] = oc_polybool(Left, L, 'OR');
    Union = polyarea(X_l, Y_l) + polyarea(X_r, Y_r)
    ACC = Inter / Union




    [X_r,Y_r,nP] = oc_polybool(Right, NEW_r, 'AND');
    [X_l,Y_l,nP] = oc_polybool(Left, NEW_l, 'AND');
    [X_I, Y_I] = oc_polybool([X_l, Y_l], [X_r, Y_r], 'OR');

    [X_r,Y_r,nP] = oc_polybool(Right, NEW_r, 'OR');
    [X_l,Y_l,nP] = oc_polybool(Left, NEW_l, 'OR');
    [X_U, Y_U] = oc_polybool([X_l, Y_l], [X_r, Y_r], 'OR');

    Inter = polyarea(X_I, Y_I)
    Union = polyarea(X_U, Y_U)
    ACC = Inter / Union


     % GX(1:100, (end - 100):end)
     % GY(1:100, (end - 100):end)
     % GR(1:100, (end - 100):end)
     % GL(1:100, (end - 100):end)

    %lungDICOM(GX,GY,GR,GL,a,b,c,d);
    %subplot(1,5,5);
    %histeq(GX);
    %G = im2bw(GX, 0.5);
    %imshow(G);

    %hy = fspecial('sobel');
    %hx = hy';
    %I = imclose(I, shape);

    %[C, h] = contour(double(I));
    %imshow(I)
    %Ix=imfilter(double(I), hx, 'replicate');
    %Iy=imfilter(double(I), hy, 'replicate');
    %I=sqrt(Ix.^2+Iy.^2);
    %shape = strel('disk', 10);
    %I = edge(I, 'canny', 0.28);
    %C = contourcs(double(I));
    %X = [];
    %Y = [];
    %for i = 1:size(C())
    %    if C(i).Level == 10000
    %        X = cat(2, X, C(i).X);
    %        Y = cat(2, Y, C(i).Y);
    %    end
    %end

    %plot(X, Y)
    %contour(xMat, yMat, I)
    %I = imclose(I, shape);
    %imshow(I)

    %hy=fspecial('gaussian', 12, 5.1);
    %hy = fspecial('sobel');
    %hx = hy';
    %J = imfilter(J, hy, 'replicate');
    %Ix=imfilter(double(I), hx, 'replicate');
    %Iy=imfilter(double(I), hy, 'replicate');
    %I=sqrt(Ix.^2+Iy.^2);
    %imshow(gradmag,[])
    %J = imclose(J, shape);
    %J = edge(J, 'canny', 0.3);
    %shape = strel('disk', 20);
    %J = imclose(J, shape);
    %subplot(1,3,2);
    %imshow(I);
    % end
% end

A = ASM;


% function gb = gabor_fn(sigma_x, sigma_y, theta, lambda, psi, gamma)

% sz_x = fix(6 * sigma_x);
% if mod(sz_x,2)==0, sz_x = sz_x + 1; end
	

% sz_y = fix(6 * sigma_y);
% if mod(sz_y, 2)==0, sz_y = sz_y + 1; end

% [x y] = meshgrid(-fix(sz_x/2):fix(sz_x/2), fix(-sz_y/2):fix(sz_y/2));

% % Поворот
% x_theta = x*cos(theta) + y*sin(theta);
% y_theta = -x*sin(theta) + y*cos(theta);

% gb = exp(-.5 * (x_theta^2/sigma_x^2 + gamma^2 * y_theta.^2/sigma_y^2))* cos(2 * pi/lambda * x_theta + psi);
end



function [gx,gy,gr,gl]=gaussgradient(IM,sigma)
%GAUSSGRADIENT Gradient using first order derivative of Gaussian.
%  [gx,gy]=gaussgradient(IM,sigma) outputs the gradient image gx and gy of
%  image IM using a 2-D Gaussian kernel. Sigma is the standard deviation of
%  this kernel along both directions.
%determine the appropriate size of kernel. The smaller epsilon, the larger
%size.
epsilon=1e-7;
halfsize=ceil(sigma*sqrt(-2*log(sqrt(2*pi)*sigma*epsilon)));
size=2*halfsize+1;
%generate a 2-D Gaussian kernel along x direction
for i=1:size
    for j=1:size
        u=[i-halfsize-1 j-halfsize-1];
        hx(i,j)=gauss(u(1),sigma)*dgauss(u(2),sigma);
    end
end
hx=hx/sqrt(sum(sum(abs(hx).*abs(hx))));
%generate a 2-D Gaussian kernel along y direction
hy=hx';

%generate hr and hl by -25 and 15 degrees accordingaly

for i=1:size
    for j=1:size
        hr(i,j)= cos(15) * hx(i,j) + sin(-15) * hy(i,j);
        hl(i,j)= cos(25) * hx(i,j) + sin(-25) * hy(i,j);
    end
end

%2-D filtering
gx=imfilter(IM,hx,'replicate','conv');
gy=imfilter(IM,hy,'replicate','conv');
gx=imfilter(gx,hx,'replicate','conv');
gy=imfilter(gy,hy,'replicate','conv');
gr=imfilter(gx,hx,'replicate','conv');
gl=imfilter(gy,hy,'replicate','conv');

function y = gauss(x,sigma)
%Gaussian
y = exp(-x^2/(2*sigma^2)) / (sigma*sqrt(2*pi));
end

function y = dgauss(x,sigma)
%first order derivative of Gaussian
y = -x * gauss(x,sigma) / sigma^2;
end

end
